File sync compare
=================

Small utility for comparing files after sync. Uses hash codes and thus needs to 
read each file. This is done in chunks (per 10 MB by default) to reduce memory 
usage. 

To use, just copy in the root of your project and let it run with

python file_sync_compare.py

Then copy / sync your project to a remote machine and run the command again. 
It automatically compares the current run with the hashcodes from the precious 
run.

There are some command line options and settings explained in 
file_sync_compare.py. 


All other files and folders here are just used for 
development/testing and are not needed to run.

Development notes
-----------------

This should stay a small utility and only use python standard packages.


""" Simple tool to compare files based on md5 checksums

Usage:

Run in the root of the project or within a folder 'sync_checks'.
The tool automatically compares the checksums with the previous run.


Possible command line arguments:

file_sync_compare file1 file2 : compare the outputs saved in file1 to file2 without doing and new checksums
file_sync_compare folder: check folder (instead of using the current directory)
file_sync_compare folder output_file: check folder and store data in output_file 

Konstantin Stadler 20201202
"""

import hashlib
import os
import getpass
import logging
import json
import platform
import time
import socket
import sys
import concurrent.futures
from pathlib import Path

# Hashing algorithm, blake2b is fast and secure (in contrast to md5)
HASHALGO = hashlib.blake2b

# Maximum size of file loaded into memory once
BLOCKSIZE = 1048576

# Subfolder for storing checksums
HASH_STORAGE = "sync_check"

# Filename for hashes root
HASH_FILENAME_ROOT = "hash_"

# Error Message start string while reading file
FILE_ERROR_MSG = "Error reading file:"


def get_hash(file):
    """Hash codes for file"""

    def file_hex_update(fo):
        fhah = HASHALGO()
        with open(fo, "rb") as ff:
            block = ff.read(BLOCKSIZE)
            while block:
                fhah.update(block)
                block = ff.read(BLOCKSIZE)
        return fhah.hexdigest()

    co = 0
    max_attempts = 5
    delay_between_attempts = 10

    while co <= max_attempts:
        try:
            file_hash = file_hex_update(file)
            break
        except Exception as e:
            co += 1
            if co == max_attempts:
                file_hash = "{msg} {exp}".format(msg=FILE_ERROR_MSG, exp=e)
                break
            # give some time for any file processes to finish
            time.sleep(delay_between_attempts)

    return file_hash


def get_ip():
    """Finds outward IP adress if connected, None otherwise"""
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("8.8.8.8", 80))
        ip = s.getsockname()[0]
        s.close()
    except:
        ip = None
    return ip


def build_hash_collector(checksums, time_info):
    """Summary dictonary with all data

    Parameters
    ----------

    checksums: dict
        filename and checksum

    time_info: dict
        start and endtimes

    Returns
    -------

    dict
    """
    os_info = dict(platform.uname()._asdict())
    os_info.pop("node")
    return {
        "hash_algorithm": HASHALGO().name,
        "computer_name": platform.node(),
        "ip": get_ip(),
        "user": getpass.getuser(),
        "timing": time_info,
        "computer_os": os_info,
        "python": platform.python_version(),
        "checksums": checksums,
    }


def dump_hash(collected_hashresult, file):
    with open(file, "w") as of:
        json.dump(collected_hashresult, of, indent=2)


def loop_for_hash(folder):
    print("Getting folder content...")
    content = [
        file
        for file in folder.glob("**/*")
        if file.is_file() and not file.stem.startswith(HASH_FILENAME_ROOT)
    ]

    print("Calculating {} hash codes: ".format(len(content)), end="", flush=True)

    hash_collector = dict()
    with concurrent.futures.ThreadPoolExecutor(max_workers=None) as executor:
        executor_jobs = {
            executor.submit(get_hash, entry): str(entry.relative_to(folder).as_posix())
            for entry in content
        }
        for ff in concurrent.futures.as_completed(executor_jobs):
            pp = executor_jobs[ff]
            print(".", end="", flush=True)
            try:
                hash_attempt = ff.result()
            except Exception as e:
                hash_attempt = "{msg} {exp}".format(msg=FILE_ERROR_MSG, exp=e)
            hash_collector[pp] = hash_attempt

    errors = {k: v for k, v in hash_collector.items() if FILE_ERROR_MSG in v}
    nr_errors = len(errors.keys())
    if nr_errors > 0:
        logging.error("No hashcode for {} file(s) - see output file.".format(nr_errors))
    print("\n")
    return hash_collector


def compare_hash_files(new_file, old_file):
    """Compare checksums from old_file with new_file

    Both files need to be json with entries
    timing - start_UTC, computer_name and a dict with checksums
    """

    with open(old_file, "r") as of:
        old = json.load(of)
    with open(new_file, "r") as nf:
        new = json.load(nf)

    print(
        "Comparing:\n"
        "OLD: {old_file} (generated on {old_start_UTC} from {old_pc}) with\n"
        "NEW: {new_file} (generated on {new_start_UTC} form {new_pc})\n".format(
            old_file=old_file.stem,
            old_start_UTC=old["timing"]["start_UTC"],
            old_pc=old["computer_name"],
            new_file=new_file.stem,
            new_start_UTC=new["timing"]["start_UTC"],
            new_pc=new["computer_name"],
        )
    )

    if old["hash_algorithm"] != new["hash_algorithm"]:
        logging.error("The two files were generated with different hash algorithm!")
        return False
    old_hash = old["checksums"]
    new_hash = new["checksums"]
    difference = {
        k: (old_hash.get(k, "not in old")[:10], new_hash.get(k, "not in new")[:10])
        for k in dict(old_hash.items() ^ new_hash.items()).keys()
    }
    errors_old = {k: v for k, v in old_hash.items() if FILE_ERROR_MSG in v}
    errors_new = {k: v for k, v in new_hash.items() if FILE_ERROR_MSG in v}

    if len(errors_old.keys()) > 0:
        print("\nFile reading errors in the previous run:")
        for k in errors_old.keys():
            print(k + " : " + " - " + errors_old[k])
    if len(errors_new.keys()) > 0:
        print("\nFile reading errors in the current run:")
        for k in errors_new.keys():
            print(k + " : " + " - " + errors_new[k])

    if len(difference.keys()) == 0:
        print("\nNo differences found")
        return True
    else:
        print("\nDifferences (file: old - new):\n")
        for k in difference.keys():
            print(k + " : " + " - ".join(difference[k]))
        return False


def sync_check(folder):
    """Compare the two latest sync files in folder"""
    jsonfiles = sorted(list(folder.glob("*.json")), reverse=True)
    if len(jsonfiles) == 0:
        logging.warning(
            "No sync data files in {} - generate hash codes first.".format(folder)
        )
        return
    elif len(jsonfiles) == 1:
        logging.warning(
            "Only one sync data file found in {} - run a second time to compare to previous run.".format(
                folder
            )
        )
        return
    compare_hash_files(new_file=jsonfiles[0], old_file=jsonfiles[1])


def run_to_hash(folder=None, output_file=None):
    """Main top-level function to generate hash and compare with previous ones

    Parameters
    ----------

    folder: pathlib.Path or str
        Folder to check, optional (default: current directory or .. if in directory ./sync_check)
    output_filer: pathlib.Path or str
        File to use to store sync hash codes, optional (default: json file based on UTC timestamp in ./sync_check)


    """

    timing = dict(
        start_UTC=time.strftime("%Y%m%d_%H%M%S", time.gmtime()),
        start_local=time.strftime("%Y%m%d_%H%M%S", time.localtime()),
    )
    if not folder:
        folder = Path(os.getcwd())
        if folder.stem == HASH_STORAGE:
            folder = check_folder.parents[0]
    else:
        folder = Path(folder)

    assert folder.is_dir(), "Pass a folder for running the checks"

    if not output_file:
        output_folder = folder / HASH_STORAGE
        output_folder.mkdir(exist_ok=True)
        output_file_name = (
            HASH_FILENAME_ROOT + timing["start_UTC"] + "_" + platform.node() + ".json"
        )
        output_file = output_folder / output_file_name
    else:
        output_file = Path(output_file)

    hashes = loop_for_hash(folder)
    timing["end_UTC"] = time.strftime("%Y%m%d_%H%M%S", time.gmtime())
    timing["end_local"] = time.strftime("%Y%m%d_%H%M%S", time.localtime())
    collected_hash = build_hash_collector(hashes, timing)
    dump_hash(collected_hashresult=collected_hash, file=output_file)
    print(
        "Finished calculating hash codes. Started at {start} - Finished at {end}\n\n".format(
            start=timing["start_local"], end=timing["end_local"]
        )
    )
    sync_check(output_file.parent)


if __name__ == "__main__":
    """For command line usage

    Within python use the function
    run_to_hash
    and/or
    compare_hash_files
    """
    if (len(sys.argv) - 1) == 0:
        run_to_hash()

    if (len(sys.argv) - 1) == 1:
        run_to_hash(folder=sys.argv[1])

    if (len(sys.argv) - 1) == 2:
        arg1 = Path(sys.argv[1])
        arg2 = Path(sys.argv[2])
        if arg1.is_file():
            compare_hash_files(arg1, arg2)
        else:
            run_to_hash(folder=arg1, output_file=arg2)
